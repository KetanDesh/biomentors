<html>
<head>
  <title>{{$data['Subject']}}</title>
  <style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-lg-12" style="margin-top: 15px ">
      
      </div>
    </div><br>
    <table class="table table-bordered">
      <tr>
        <th>Subject</th>
        <th>Topic</th>
        <th>Questions</th>
      </tr>
      
      <tr>
        <td>{{$data['Subject']}}</td>
        <td>{{$data['Topic']}}</td>
        <td>{{$data['Questions']}}
        </td>
      </tr>
    </table>
  </div>
</body>
</html>