<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Controllers\Api\PdfCreationController;
use Illuminate\Support\Facades\View;
use App\Models\PdfCreation;
use File;
use Dompdf\Dompdf;
use PDF;
use DB;

class QuestionPdfCreation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    protected $details;
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $data = DB::table("test")
        ->select('id','Subject','Topic','Questions')
        ->where("id",$this->details['unique_id'])
        ->first();
      
        $unique_id = $data->id;
        $pdf_data['Subject'] = $data->Subject;
        $pdf_data['Topic'] = $data->Topic;
        $pdf_data['Questions']= $data->Questions;

        $html = View::make ('quastion_pdf')->with ( 'data', $pdf_data );
        $dompdf = new Dompdf ();
        
        $dompdf->loadHtml ( $html );
        $customPaper = $customPaper = array (0,0,920,768 );
        $dompdf->set_paper ( $customPaper );
        $dompdf->render ();

        $pdfName = public_path()."/pdf/".$unique_id."-".$data->Subject."-".$data->Topic.".pdf";
        $upload_file = file_put_contents($pdfName, $dompdf->output());
        if(file_exists($pdfName)){
            $pdf_path = $this->details['host']."/pdf/".$unique_id."-".$data->Subject."-".$data->Topic.".pdf";
            $update = DB::table("test")->where("id",$unique_id)->update(['Pdf_link'=>$pdf_path]);
            echo "PDF  Created";
        } else {
            echo "PDF Not Created";
        }
        return;
    }
}
