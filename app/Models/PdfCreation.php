<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class PdfCreation extends Model
{
    use HasFactory;
    public function getPendingPdfCreationData(){
        $data = DB::select("SELECT id FROM `test` WHERE TIMESTAMPDIFF(MINUTE,now(),scheduled_on) =5 AND  Pdf_link is null");
        return $data;
    }
}
