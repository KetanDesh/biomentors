<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PdfCreation;

class PdfCreationController extends Controller
{
    public function __construct() {
        $this->pdfModel = new PdfCreation();
    }
    public function pdfCreation(Request $request){
        $host= request()->getSchemeAndHttpHost();
        $pending_data = $this->pdfModel->getPendingPdfCreationData();
        $details = array();
        foreach($pending_data as $key=>$questions){
            $details['unique_id'] = $questions->id;
            $details['host'] = $host;
            // set job for pdf creation
            dispatch(new \App\Jobs\QuestionPdfCreation($details)); 
        }
        return $pending_data;
    }
}
